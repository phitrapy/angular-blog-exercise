import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  blogPosts = [
    { 
      title: 'Post sympa',
      content: 'Ce premier billet est à priori sympa... ou pas.',
      loveIts: 0,
      created_at: new Date()
    },
    { 
      title: 'Post bien nul comme il faut',
      content: 'Ce premier billet est nul!',
      loveIts: -100,
      created_at: new Date()
    },
    { 
      title: 'Post qui déchire',
      content: 'Ce premier billet est beaucoup trop bien!',
      loveIts: +100,
      created_at: new Date()
    },
  ];

}
